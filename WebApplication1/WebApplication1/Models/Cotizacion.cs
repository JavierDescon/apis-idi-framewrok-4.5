﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Cotizacion
    {
        
        public string DocNum { get; set; }
        public string CardName;
        public string DocTotal;
        public string DocDate;
        public string CardCode;
        public string DocEntry;
        public string TipoDireccion;
        public string OC;

    }
    
    public class Lineas
    {
        public int LineNum = 0;
        public double quantity = 0;
        public string ItemCode = "";
        public double price = 0;
        public double LineTotal = 0;
        public string Descripcion = "";
        public int multiplo=0;
    }

    public class CreaCotizacion
    {
        public string CardCode;
        public string DocDate;
        public string SlpCode;
        public string OC;
        public string TipoDireccion;
        public List<Lineas> lineas;
    }

    public class VerCotizacion
    {
        public Cotizacion cotizacion;
        public List<Lineas> Lineas;
    }

}