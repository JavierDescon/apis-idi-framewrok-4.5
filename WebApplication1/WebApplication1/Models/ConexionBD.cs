﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ConexionBD
    {

        public static SqlConnection Conectar(string server, string bd, string user, string pass)
        {
            SqlConnection conexion = new SqlConnection("server='" + server + "';database='" + bd + "';uid='" + user + "';password='" + pass + "'"); //Cadena de conexion para las consultas
            conexion.Open();//Se abre la conexion para consumir la BD
            return conexion;
        }
    }
}