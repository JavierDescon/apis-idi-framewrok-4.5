﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RMI
{
    public interface Metodos
    {
        SqlConnection getConexion();
        List<Object> getList();
        int getint();

        string postCotizacion(string json);
    }
}
