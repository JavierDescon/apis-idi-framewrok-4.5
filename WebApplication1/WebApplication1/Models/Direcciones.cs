﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Direcciones
    {
        public string tipoDireccion { get; set; }
        public string address { get; set; }
        public string zipCode { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string street { get; set; }
        public string AdresType { get; set; }

        public Direcciones() { }
    }
}