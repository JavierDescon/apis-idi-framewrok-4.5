﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{

    [Authorize]
    public class DireccionesController : ApiController
    {
        string server;
        string user;
        string bd;
        string pass;
        public DireccionesController()
        {
            server = ConfigurationManager.AppSettings["ServidorSQL"];
            user = ConfigurationManager.AppSettings["UsuarioSQL"];
            bd = ConfigurationManager.AppSettings["database"];
            pass = ConfigurationManager.AppSettings["PasswordSQL"];
        }

        public IEnumerable<Direcciones> getDirecciones(string cardCode)
        {
            List<Direcciones> listaDirecciones = new List<Direcciones>();
            //SqlCommand query = new SqlCommand("select ISNULL(T0.Address,'')'TipoDireccion', ISNULL( t1.Address,'')'Address',t1.ZipCode,isnull(t1.City,'')'City',t1.Country,t0.Street " +
            //    "from crd1 T0 " +
            //    "inner join OCRD T1 on T0.CardCode = T1.CardCode " +
            //    "Where T0.CardCode = '" + cardCode + "'", ConexionBD.Conectar(server, bd, user, pass));

            SqlCommand query = new SqlCommand("select T0.AdresType, ISNULL(T0.Address,'')'TipoDireccion', ISNULL( t1.Address,'')'Address',t1.ZipCode,isnull(t1.City,'')'City',t1.Country, t0.Street " +
                "from crd1 T0 " +
                "inner join OCRD T1 " +
                "on T0.CardCode = T1.CardCode " +
                "Where T0.CardCode = '"+ cardCode + "'", ConexionBD.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Direcciones direccionTemp = new Direcciones();
                direccionTemp.AdresType = read[0].ToString();
                direccionTemp.tipoDireccion = read[1].ToString();
                direccionTemp.address = read[2].ToString();
                direccionTemp.zipCode = read[3].ToString();
                direccionTemp.city = read[4].ToString();
                direccionTemp.country = read[5].ToString();
                direccionTemp.street = read[6].ToString();
                
                listaDirecciones.Add(direccionTemp);
            }
            return listaDirecciones;
        }
    }
}
