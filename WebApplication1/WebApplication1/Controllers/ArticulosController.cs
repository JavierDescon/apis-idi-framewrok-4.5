﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class ArticulosController : ApiController
    {
        string server;
        string user;
        string bd;
        string pass;
        public string rutaImagen;
        public ArticulosController()
        {
            server = ConfigurationManager.AppSettings["ServidorSQL"];
            user = ConfigurationManager.AppSettings["UsuarioSQL"];
            bd = ConfigurationManager.AppSettings["database"];
            pass = ConfigurationManager.AppSettings["PasswordSQL"];
            rutaImagen = ConfigurationManager.AppSettings["rutaImagen"];
        }
        public IEnumerable<Articulos> get()
        {
            List<Articulos> listaArticulos = new List<Articulos>();
            SqlCommand query = new SqlCommand("select * from ospp;", ConexionBD.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Articulos articuloTemp = new Articulos();
                articuloTemp.ItemCode = read[0].ToString();
                articuloTemp.Price = read[2].ToString();
            
                articuloTemp.foto = articuloTemp.foto = EncodeImage(articuloTemp.ItemCode); 

                listaArticulos.Add(articuloTemp);
            }
            return listaArticulos;
        }

        [HttpGet]
        public IEnumerable<Articulos> getArticulos(string cardCode) //filra articulos por vendededor
        {
            List<Articulos> listaArticulos = new List<Articulos>();
            SqlCommand query = new SqlCommand("select oitm.ItemName,oitm.ItemCode,Price,oitm.SalPackUn " +
                "from ospp inner join oitm " +
                "on ospp.ItemCode = oitm.ItemCode " +
                "where ospp.CardCode = '"+cardCode+"';", ConexionBD.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Articulos articuloTemp = new Articulos();
                articuloTemp.ItemName = read["ItemName"].ToString();
                articuloTemp.ItemCode = read["ItemCode"].ToString();
                articuloTemp.Price = read["Price"].ToString();
                articuloTemp.Multiplo = Convert.ToInt16(read["SalPackUn"]);
                articuloTemp.foto = EncodeImage(articuloTemp.ItemCode);
                listaArticulos.Add(articuloTemp);
            }
            return listaArticulos;
        }


      
        private string EncodeImage(string nombreImagen)
        {

            try
            {
                byte[] imageArray = System.IO.File.ReadAllBytes(rutaImagen + "\\" + nombreImagen + ".jpg");
                return Convert.ToBase64String(imageArray);
            }
            catch (Exception ex)
            {
                return "Foto no disponible";
            }

        }

        //[HttpGet]
        //public IEnumerable<Descuento> getPrecioArticulos([FromUri]string ItemCode, [FromUri]string CardCode)
        //{
        //    List<Descuento> listaArticulos = new List<Descuento>();
        //    SqlCommand query = new SqlCommand("select t1.Price , Discount ,  (T1.Price *  (100-T0.Discount)/100) [Precio Tras Descuento], t0.ItemCode  " +
        //        "from OSPP T0 Inner Join ITM1 T1 oN T0.ItemCode = T1.ItemCode " +
        //        "wHERE t0.ItemCode = '" + ItemCode + "' and PriceList = '2' and T0.CardCode = '" + CardCode + "'; ", ConexionBD.Conectar(server, bd, user, pass));
        //    SqlDataReader read = query.ExecuteReader();
        //    while (read.Read())
        //    {
        //        Descuento descuentoTemp = new Descuento();
        //        descuentoTemp.itemCode = read[3].ToString();
        //        descuentoTemp.precio = read[0].ToString();
        //        descuentoTemp.descuento = read[1].ToString();
        //        descuentoTemp.precio_tras_descuento = read[2].ToString();
        //    }
        //    return listaArticulos;
        //}

        /// 
        ///	  Codifica una imagen en Base64
        /// 
        //private string EncodeImage(string nombreImagen)
        //{
        //    string ruta = ConfigurationSettings.AppSettings["RutaImagenes"];
        //    try
        //    {
        //        byte[] imageArray = System.IO.File.ReadAllBytes(ruta + "\\" + nombreImagen + ".jpg");
        //        return Convert.ToBase64String(imageArray);
        //    }
        //    catch (Exception)
        //    {

        //        return "Foto no disponible";
        //    }

        //}
    }
//}
}
