﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using WebApplication1.Models;
using AuthorizeAttribute = System.Web.Http.AuthorizeAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class CotizacionController : ApiController
    {

        string server;
        string user;
        string bd;
        string pass;

        public string sErrMsg;
        public int lErrCode;
        public int lRetCode;
        public Boolean Conectado = false;
        public Company oCompany;

        public CotizacionController()
        {
            server = ConfigurationManager.AppSettings["ServidorSQL"];
            user = ConfigurationManager.AppSettings["UsuarioSQL"];
            bd = ConfigurationManager.AppSettings["database"];
            pass = ConfigurationManager.AppSettings["PasswordSQL"];
            
        }

        private Resultado ConectaSAP(int tipo)
        {
            Resultado response = new Resultado();
            try
            {
                server = ConfigurationSettings.AppSettings["ServidorSQL"];
                user = ConfigurationSettings.AppSettings["UsuarioSQL"];
                bd = ConfigurationSettings.AppSettings["database"];
                pass = ConfigurationSettings.AppSettings["PasswordSQL"];
                string LicenseServer = ConfigurationSettings.AppSettings["LicenseServer"];
                string DbUserName = ConfigurationSettings.AppSettings["usuario"];
                string DbPassword = ConfigurationSettings.AppSettings["pass"];
                string TipoSQL = ConfigurationSettings.AppSettings["TipoSQL"];
                

                oCompany = new Company();

                oCompany.Server = server;
                oCompany.LicenseServer = LicenseServer;
                oCompany.DbUserName = user;
                oCompany.DbPassword = pass;

                if (TipoSQL == "2008")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008);
                }
                else if (TipoSQL == "2012")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012);
                }
                else if (TipoSQL == "2014")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014);
                }
                else if (TipoSQL == "2016")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016);
                }

                oCompany.UseTrusted = false;
                oCompany.CompanyDB = bd;
                oCompany.UserName = DbUserName;
                oCompany.Password = DbPassword;

                // Connecting to a company DB
                lRetCode = oCompany.Connect();

                if (lRetCode != 0)
                {
                    int temp_int = lErrCode;
                    string temp_string = sErrMsg;
                    oCompany.GetLastError(out temp_int, out temp_string);
                    response.error = true;
                    response.message = sErrMsg;
                    //MessageBox.Show(globals_Renamed.sErrMsg);
                }
                else
                {

                    response.error = false;
                    response.message = "Conectado";
                    // Disable controls

                }

            }
            catch (Exception ex)
            {
                response.error = true;
                response.message = ex.Message;
            }
            return response;

        }

        public IEnumerable<Cotizacion> getCotizaciones(int SlpCode) // listado de cotizaciones por vendedor 
        {
            List<Cotizacion> listaCotizaciones = new List<Cotizacion>();
            SqlCommand query = new SqlCommand("SELECT T1.DocEntry, T1.DocNum, T1.CardCode, T1.CardName,T1.DocDate,t1.DocTotal,T1.ShipToCode,t1.shipToCode " +
                "fROM OQUT T1 " +
                "WHERE T1.DocStatus = 'O' AND T1.SlpCode ='"+SlpCode+"'" +
                "ORDER BY T1.DocDate desc", ConexionBD.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Cotizacion cotizacionTemp = new Cotizacion();

                cotizacionTemp.DocEntry= read[0].ToString();
                cotizacionTemp.DocNum = read[1].ToString();
                cotizacionTemp.CardCode = read[2].ToString();
                cotizacionTemp.CardName = read[3].ToString();
                cotizacionTemp.DocDate= read[4].ToString();
                cotizacionTemp.DocTotal= read[5].ToString();
                cotizacionTemp.TipoDireccion = read[6].ToString();
                listaCotizaciones.Add(cotizacionTemp);
            }
            return listaCotizaciones;
        }

        public VerCotizacion getCotizacioneEdita(string DocEntry)
        {
            VerCotizacion cotizacion = new VerCotizacion();
            List<Lineas> linea = new List<Lineas>();
            SqlCommand query = new SqlCommand("Select  t0.CardCode,t0.CardName,t0.DocDate,t0.NumAtCard,t1.LineNum,t1.ItemCode,(Select SalPackUn from OITM where ItemCode=t1.ItemCode)'multiplo' ,t1.Dscription,t1.Quantity,t1.Price,t1.LineTotal,t0.DocTotal, t1.shipToCode " +
                "from Oqut t0 " +
                "inner join QUT1 t1 on t0.DocEntry=t1.DocEntry " +
                "where t0.DocEntry=" + DocEntry, ConexionBD.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Cotizacion cotizacionTemp = new Cotizacion();
                cotizacionTemp.DocEntry = DocEntry;
                cotizacionTemp.CardCode = read[0].ToString();
                cotizacionTemp.CardName = read[1].ToString();
                cotizacionTemp.DocDate = read[2].ToString();
                cotizacionTemp.DocTotal = read["DocTotal"].ToString();
                cotizacionTemp.TipoDireccion = read["ShipToCode"].ToString();
                cotizacionTemp.OC = read["NumAtCard"].ToString();

                Lineas lineas = new Lineas();
                lineas.LineNum = Convert.ToInt16(read["LineNum"]);
                lineas.ItemCode = read["ItemCode"].ToString();
                lineas.price = Convert.ToDouble(read["Price"]);
                lineas.Descripcion = read["Dscription"].ToString();
                lineas.quantity = Convert.ToDouble(read["Quantity"]);
                lineas.multiplo = Convert.ToInt32( read["multiplo"]);
                lineas.LineTotal = lineas.price * lineas.quantity;
                linea.Add(lineas);

                cotizacion.cotizacion = cotizacionTemp;
                cotizacion.Lineas = linea;
            }
            return cotizacion;
        }

        [HttpPost]
        public Resultado PostCotizacion(CreaCotizacion json)
        {

            Resultado response = new Resultado();
            string CardCode = json.CardCode;
            string DocDate = json.DocDate;
            string SlpCode = json.SlpCode;
            string ShipToCode = json.TipoDireccion;
            string OC = json.OC;
            var respuesta = ConectaSAP(1);
            if (respuesta.error == false)
            {
               

                Documents OCotizacion;
               
                OCotizacion = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oQuotations);
              

                OCotizacion.CardCode = CardCode;
                OCotizacion.DocDate = Convert.ToDateTime(DocDate);
                OCotizacion.SalesPersonCode = Convert.ToInt32(SlpCode);
                OCotizacion.ShipToCode = ShipToCode;
                OCotizacion.NumAtCard = OC; // "Cotización creada desde el Portal";

                //Aqui creamos las lineas
                foreach (var linea in json.lineas)
                {
                    string ItemCode = linea.ItemCode;
                    double quantity = linea.quantity;
                    double price = linea.price;
                    OCotizacion.Lines.TaxCode = "A24";
                    OCotizacion.Lines.ItemCode = ItemCode;
                    OCotizacion.Lines.Quantity = quantity;
                    OCotizacion.Lines.Price = price;
                    OCotizacion.Lines.Add();
                }
                //Agregar Documento 
                lErrCode = OCotizacion.Add();
                
                if (lErrCode != 0)
                {
                    int temp_int = lErrCode;
                    string temp_string = sErrMsg;
                    oCompany.GetLastError(out temp_int, out temp_string);
                    response.error = true;
                    response.message = temp_string;


                    //Poner mensaje de error que lea esto
                    //resultado = temp_string;
                    //MessageBox.Show(temp_string);
                }
                else
                {
                    //Aqui cuando se crea la APS correctamente
                    // = "APS was added successfully";

                    oCompany.Disconnect();
                    response.error = false;
                    response.message = "Cotización generada con exíto";
                }

            }
            else
            {
                response.error = true;
                response.message = respuesta.message;
            }

            return response;
        }

        
    }
}
