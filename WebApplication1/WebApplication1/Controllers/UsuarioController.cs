﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class UsuarioController : ApiController
    {
        
        List<Usuario> listaUsuario = new List<Usuario>();
        public UsuarioController()
        {
           
        }
       
        public List<Usuario> getUsuarios()
        {
           string server = ConfigurationManager.AppSettings["ServidorSQL"];
            string user = ConfigurationManager.AppSettings["UsuarioSQL"];
            string bd = ConfigurationManager.AppSettings["database"];
            string pass = ConfigurationManager.AppSettings["PasswordSQL"];
            SqlConnection conexion = new SqlConnection("server='" + server + "'; database='" + bd + "'; uid='" + user + "'; password='" + pass + "'");
            conexion.Open();
            SqlCommand query = new SqlCommand("select SlpCode,SlpName,U_UsuarioWeb,U_PassWeb,U_CrearPedido from OSLP where SlpCode <> '' and SlpCode <> -1 and U_UsuarioWeb <>'' and U_PassWeb <> ''", conexion);
            //SqlCommand query = new SqlCommand("SELECT SlpCode,SlpName,U_UsuarioWeb,U_PassWeb FROM OSLP;", ConexionBd.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Usuario usuarioTemp = new Usuario();
                usuarioTemp.SlpCode = read[0].ToString();
                usuarioTemp.SLPName = read[1].ToString();
                usuarioTemp.UsuarioWeb=read[2].ToString();
                usuarioTemp.PassWeb= Encrypt.Base64_Encode(read[3].ToString());
                if (read["U_CrearPedido"].ToString()=="Y")
                {
                    usuarioTemp.CrearPedido = true;
                }
                else
                {
                    usuarioTemp.CrearPedido = false;
                }

                listaUsuario.Add(usuarioTemp);
            }
            conexion.Close();
            return listaUsuario;
        }
        public class Encrypt
        {
            //codificar base64
            public static string Base64_Encode(string str)
            {
                byte[] encbuff = System.Text.Encoding.UTF8.GetBytes(str);
                return Convert.ToBase64String(encbuff);
            }

            //Decodificar base64
            public static string Base64_Decode(string str)
            {
                try
                {
                    byte[] decbuff = Convert.FromBase64String(str);
                    return System.Text.Encoding.UTF8.GetString(decbuff);
                }
                catch
                {
                    //si se envia una cadena si codificación base64, mandamos vacio
                    return "";
                }
            }
        }

        [HttpGet]
        public List<Cliente> getClientes(int SlpCode)
        {
            string server = ConfigurationManager.AppSettings["ServidorSQL"];
            string user = ConfigurationManager.AppSettings["UsuarioSQL"];
            string bd = ConfigurationManager.AppSettings["database"];
            string pass = ConfigurationManager.AppSettings["PasswordSQL"];
            SqlConnection conexion = ConexionBD.Conectar(server, bd, user, pass);

            List<Cliente> listaClientes = new List<Cliente>();
            SqlCommand query = new SqlCommand("  SELECT * FROM OCRD Where SlpCode = '" + SlpCode + "'; ", conexion);
            SqlDataReader read = query.ExecuteReader();
            while (read.Read()) //Se agregan todos y cada uno de los registros obtenidos a la lista clientes
            {
                Cliente clienteTemp = new Cliente();
                clienteTemp.cardCode = read["CardCode"].ToString();
                clienteTemp.cardName = read["CardName"].ToString();
                listaClientes.Add(clienteTemp);
            }
            read.Close();
            conexion.Close();
            return listaClientes; //Retornamos la lista clientes
        }
    }
}
