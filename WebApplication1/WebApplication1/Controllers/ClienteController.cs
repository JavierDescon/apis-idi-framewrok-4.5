﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class ClienteController : ApiController
    {
        string server;
        string user;
        string bd;
        string pass;
        List<Cliente> listaClientes;
        public ClienteController()
        {
            server = ConfigurationManager.AppSettings["ServidorSQL"];
            user = ConfigurationManager.AppSettings["UsuarioSQL"];
            bd = ConfigurationManager.AppSettings["database"];
            pass = ConfigurationManager.AppSettings["PasswordSQL"];
        }

        [HttpGet]
        public List<Cliente> getClientes() //se filtran por CardType ="C" y nombre de carta !=NULL
        {
            listaClientes = new List<Cliente>();
            SqlCommand query = new SqlCommand("SELECT CardCode,CardName,CardType FROM OCRD WHERE CardType = 'C' and CardName IS NOT NULL;", ConexionBD.Conectar(server, bd, user, pass));//Query para generar consulta
            SqlDataReader read = query.ExecuteReader();
            while (read.Read()) //Se agregan todos y cada uno de los registros obtenidos a la lista clientes
            {
                Cliente clienteTemp = new Cliente();
                clienteTemp.cardCode = read[0].ToString();
                clienteTemp.cardName = read[1].ToString();
                listaClientes.Add(clienteTemp);
            }
            return listaClientes; //Retornamos la lista clientes
        }

        // GET api/values

        [HttpGet]
        public IEnumerable<Cliente> getCliente(string DocEntry) //obetner clientes de una cotizacion
        { //Metodo GET con parametro como filtro de consulta donde =>   DocEntry=@param1
              listaClientes = new List<Cliente>();
            SqlCommand query = new SqlCommand("SELECT CardCode,CardName,CardType FROM OCRD WHERE CardType = 'C' and CardName IS NOT NULL and DocEntry =" + DocEntry + ";", ConexionBD.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Cliente clienteTemp = new Cliente();
                clienteTemp = new Cliente();
                clienteTemp.cardCode = read[0].ToString();
                clienteTemp.cardName = read[1].ToString();
                listaClientes.Add(clienteTemp);
            }
            return listaClientes;
        }

        [HttpGet]
        public List<Cliente> getClientes(int SlpCode) //clientes por vendedor
        {
            List<Cliente> clientes = new List<Cliente>();
            
            SqlCommand query = new SqlCommand("select distinct T1.CardCode, T1.CardName from crd1 T0 inner join OCRD T1 on T0.CardCode = T1.CardCode where U_SlpCode ='" + SlpCode+"'", ConexionBD.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Cliente cliente = new Cliente();
                cliente.cardCode = read[0].ToString();
                cliente.cardName = read[1].ToString();// +" - "+ read[2].ToString();
                //cliente.Address = read[2].ToString();
                clientes.Add(cliente);
            }
            return clientes;
        }

        //Obtener articulos por cliente
        //private List<Articulos> getArticulosCliente(Cliente cliente)
        //{
        //    SqlConnection conexion = ConexionBD.Conectar(server, bd, user, pass);

        //    List<Articulos> listaArticulos = new List<Articulos>();
        //    SqlCommand query = new SqlCommand("select * from ospp where CardCode = '" + cliente.cardCode + "'; ", conexion);
        //    SqlDataReader read = query.ExecuteReader();
        //    while (read.Read())
        //    {
        //        Articulos articuloTemp = new Articulos();
        //        articuloTemp.ItemCode = read[0].ToString();
        //        articuloTemp.CardCode = read[1].ToString();
        //        articuloTemp.Price = read[2].ToString();
        //        articuloTemp.Currency = read[3].ToString();
        //        articuloTemp.Discount = read[4].ToString();
        //        articuloTemp.ListNum = read[5].ToString();
        //        articuloTemp.AutoUpdt = read[6].ToString();
        //        articuloTemp.EXPAND = read[7].ToString();
        //        articuloTemp.UserSign = read[8].ToString();
        //        articuloTemp.SrcPrice = read[9].ToString();
        //        articuloTemp.LogInstanc = read[10].ToString();
        //        articuloTemp.UserSign2 = read[11].ToString();
        //        articuloTemp.CreateDate = read[12].ToString();
        //        articuloTemp.UpdateDate = read[13].ToString();
        //        articuloTemp.Valid = read[14].ToString();
        //        articuloTemp.ValidFrom = read[15].ToString();
        //        articuloTemp.ValidTo = read[16].ToString();
        //        listaArticulos.Add(articuloTemp);
        //    }
        //    conexion.Close();
        //    return listaArticulos;
        //}
    }
}
