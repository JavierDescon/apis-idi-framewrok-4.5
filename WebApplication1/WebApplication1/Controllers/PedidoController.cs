﻿using SAPbobsCOM;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class PedidoController : ApiController
    {
        string server;
        string user;
        string bd;
        string pass;
        List<PedidoPerzonaizado> listaPedidos;
        public PedidoController() {
            server = ConfigurationManager.AppSettings["ServidorSQL"];
            user = ConfigurationManager.AppSettings["UsuarioSQL"];
            bd = ConfigurationManager.AppSettings["database"];
            pass = ConfigurationManager.AppSettings["PasswordSQL"];
        }
        // GET: api/Pedido
        public IEnumerable<PedidoPerzonaizado> getPedidos()
        {
           
            listaPedidos = new List<PedidoPerzonaizado>();
            List<Lineas> linea = new List<Lineas>();
            SqlCommand query = new SqlCommand("SELECT DocEntry,DocNum,DocDate,cardCode,CardName,DocTotal,ORDR.SlpCode FROM ORDR INNER JOIN OSLP ON ORDR.SlpCode = OSLP.SlpCode;", ConexionBD.Conectar(server, bd, user, pass));
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                    PedidoPerzonaizado pedidoTemp = new PedidoPerzonaizado();
                    pedidoTemp.docEntry = read[0].ToString();
                    pedidoTemp.docNum = read[1].ToString();
                    pedidoTemp.docDate = read[2].ToString();
                    pedidoTemp.cardCode = read[3].ToString();
                    pedidoTemp.cardName = read[4].ToString();
                    pedidoTemp.docTotal = read[5].ToString();
                    pedidoTemp.lineas = obtenerLineas(pedidoTemp.docEntry);
                    listaPedidos.Add(pedidoTemp);
            }
            return listaPedidos;
        }

        private List<Lineas> obtenerLineas(string docEntry)
        {
            List<Lineas> linea = new List<Lineas>();
            verPedido pedido = new verPedido();
            SqlConnection conexion = ConexionBD.Conectar(server, bd, user, pass);
            SqlCommand query = new SqlCommand("Select  t0.CardCode,t0.CardName,t0.DocDate,t1.LineNum,t1.ItemCode,t1.Dscription,t1.Quantity,t1.Price,t1.LineTotal,t0.DocTotal,t0.VatPercent " +
                "from ORDR t0 " +
                "inner join RDR1 t1 " +
                "on t0.DocEntry=t1.DocEntry " +
                "where t0.DocEntry='" + docEntry +"'", conexion);
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Lineas lineas = new Lineas();
                lineas.LineNum = Convert.ToInt16(read["LineNum"]);
                lineas.ItemCode = read["ItemCode"].ToString();
                lineas.price = Convert.ToDouble(read["Price"]);
                lineas.Descripcion = read["Dscription"].ToString();
                lineas.quantity = Convert.ToDouble(read["Quantity"]);
                lineas.LineTotal = lineas.price * lineas.quantity;
                linea.Add(lineas);
            }
            conexion.Close();
            return linea;
        }

        public verPedido getPedido(int docEntry) {

            verPedido pedido = new verPedido();
            List<Lineas> linea = new List<Lineas>();
            SqlConnection conexion = ConexionBD.Conectar(server, bd, user, pass);
            SqlCommand query = new SqlCommand("Select  t0.CardCode,t0.CardName,t0.DocDate,t1.LineNum,t1.ItemCode,t1.Dscription,t1.Quantity,t1.Price,t1.LineTotal,t0.DocTotal,t0.VatSum,t0.comments,t0.shipToCode " +
                "from ORDR t0 " +
                "inner join RDR1 t1 " +
                "on t0.DocEntry=t1.DocEntry " +
                "where t0.DocEntry='"+docEntry+"'", conexion);
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                Pedido PedidoTemp = new Pedido();
                PedidoTemp.docEntry = ""+docEntry;
                PedidoTemp.CardCode = read[0].ToString();
                PedidoTemp.CardName = read[1].ToString();
                PedidoTemp.DocDate = read[2].ToString();
                PedidoTemp.DocTotal = read["DocTotal"].ToString();
                PedidoTemp.IVA = read["vatSum"].ToString();
                PedidoTemp.TipoDireccion = read["shipToCode"].ToString();
                PedidoTemp.Comments = read["Comments"].ToString();

                Lineas lineas = new Lineas();
                lineas.LineNum = Convert.ToInt16(read["LineNum"]);
                lineas.ItemCode = read["ItemCode"].ToString();
                lineas.price = Convert.ToDouble(read["Price"]);
                lineas.Descripcion = read["Dscription"].ToString();
                lineas.quantity = Convert.ToDouble(read["Quantity"]);
                lineas.LineTotal = lineas.price * lineas.quantity;
                linea.Add(lineas);

                pedido.pedido = PedidoTemp;
                pedido.Lineas = linea;
            }
            conexion.Close();
            return pedido;
        }

        public List<verPedido> getPedido(string slpCode)
        {
            List<verPedido> listPedidos = new List<verPedido>();
           
            SqlConnection conexion = ConexionBD.Conectar(server, bd, user, pass);
            SqlCommand query = new SqlCommand("Select  t0.CardCode,t0.CardName,t0.DocDate,t1.LineNum,t1.ItemCode,t1.Dscription,t1.Quantity,t1.Price,t1.LineTotal,t0.DocTotal,t0.VatSum,t0.Comments,t0.docEntry,t0.shipToCode " +
                "from ORDR t0 " +
                "inner join RDR1 t1 " +
                "on t0.DocEntry=t1.DocEntry " +
                "where t0.slpCode='" + slpCode + "'", conexion);
            SqlDataReader read = query.ExecuteReader();
            string docEntry="";
            while (read.Read())
            {
                verPedido pedido = new verPedido();
                Pedido PedidoTemp = new Pedido();

                PedidoTemp.CardCode = read[0].ToString();
                PedidoTemp.CardName = read[1].ToString();
                PedidoTemp.DocDate = read[2].ToString();
                PedidoTemp.DocTotal = read["DocTotal"].ToString();
                PedidoTemp.IVA = read["vatSum"].ToString();
                PedidoTemp.Comments = read["Comments"].ToString();
                docEntry = read["DocEntry"].ToString();
                PedidoTemp.TipoDireccion = read["shipToCode"].ToString();
                PedidoTemp.docEntry = docEntry;

                //Lineas lineas = new Lineas();
                //lineas.LineNum = Convert.ToInt16(read["LineNum"]);
                //lineas.ItemCode = read["ItemCode"].ToString();
                //lineas.price = Convert.ToDouble(read["Price"]);
                //lineas.Descripcion = read["Dscription"].ToString();
                //lineas.quantity = Convert.ToDouble(read["Quantity"]);
                //lineas.LineTotal = lineas.price * lineas.quantity;
                //linea.Add(lineas);

                var pedidoExiste = listPedidos.Find(ped => ped.pedido.docEntry == docEntry);
                if (pedidoExiste==null)
                {
                    pedido.pedido = PedidoTemp;
                    pedido.Lineas = obtenerLineas(docEntry);
                    listPedidos.Add(pedido);
                }
                else {
                    pedidoExiste.Lineas = obtenerLineas(docEntry);
                }
            }
            conexion.Close();
            return listPedidos;
        }




        public string sErrMsg;
        public int lErrCode;
        public int lRetCode;
        public bool conectado = false;
        public Company oCompany;



        [HttpPost]
        public Resultado PostPedido(creaPedido pedido) {

            Resultado response = new Resultado();
            int DocEntry = Convert.ToInt32(pedido.DocEntry);
            string CardCode = pedido.CardCode;
            string DocDate = pedido.DocDate;
            string DocDueDate = pedido.DocDueDate;
            string SlpCode = pedido.SlpCode;
            string ShipToCode = pedido.TipoDireccion;
            string comentarios = "Basado en oferta de ventas " + DocEntry;

            if (conectado==false)
            {
                ConectaSAP();
            }
            Documents OPedido;
            OPedido = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oOrders);
            
            OPedido.CardCode = CardCode;
            
            OPedido.DocDate = Convert.ToDateTime(DocDate);
            OPedido.DocDueDate = Convert.ToDateTime(DocDueDate);
            OPedido.SalesPersonCode = Convert.ToInt32(SlpCode);
            OPedido.Comments = comentarios;
            OPedido.ShipToCode = ShipToCode;
            //OPedido.NumAtCard = "Cotización creada desde el Portal";

            foreach (var linea in pedido.lineas)
            {
               

                //OPedido.Lines.TaxCode = "B16";
                OPedido.Lines.ItemCode = linea.ItemCode;
                OPedido.Lines.Quantity = linea.quantity;
                //OPedido.Lines.Price = linea.price;

                OPedido.Lines.BaseType = 23;
                OPedido.Lines.BaseLine = linea.LineNum;
                OPedido.Lines.BaseEntry = DocEntry;

                OPedido.Lines.Add();
            }
            lErrCode = OPedido.Add();
            if (lErrCode!=0)
            {
                int temp_int= lErrCode;
                string temp_string = sErrMsg;
                oCompany.GetLastError(out temp_int, out temp_string);
                response.error = true;
                response.message = temp_string;
            }
            else
            {
                oCompany.Disconnect();
                response.error = false;
                response.message = "Pedido Generado con extito";
            }

            return response;
            
        }
        private void ConectaSAP()
        {
            try
            {
                server = ConfigurationSettings.AppSettings["ServidorSQL"];
                user = ConfigurationSettings.AppSettings["UsuarioSQL"];
                bd = ConfigurationSettings.AppSettings["database"];
                pass = ConfigurationSettings.AppSettings["PasswordSQL"];
                string LicenseServer = ConfigurationSettings.AppSettings["LicenseServer"];
                string DbUserName = ConfigurationSettings.AppSettings["usuario"];
                string DbPassword = ConfigurationSettings.AppSettings["pass"];
                string TipoSQL = ConfigurationSettings.AppSettings["TipoSQL"];


                oCompany = new Company();

                oCompany.Server = server;
                oCompany.LicenseServer = LicenseServer;
                oCompany.DbUserName = user;
                oCompany.DbPassword = pass;

                if (TipoSQL == "2008")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008);
                }
                else if (TipoSQL == "2012")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012);
                }
                else if (TipoSQL == "2014")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014);
                }
                else if (TipoSQL == "2016")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016);
                }

                oCompany.UseTrusted = false;
                oCompany.CompanyDB = bd;
                oCompany.UserName = DbUserName;
                oCompany.Password = DbPassword;

                // Connecting to a company DB
                lRetCode = oCompany.Connect();

                if (lRetCode != 0)
                {
                    int temp_int = lErrCode;
                    string temp_string = sErrMsg;
                    oCompany.GetLastError(out temp_int, out temp_string);
                    //MessageBox.Show(globals_Renamed.sErrMsg);
                }
                else
                {

                    conectado = true;
                    // Disable controls

                }

            }
            catch (Exception ex)
            {

            }

        }

    }
}
