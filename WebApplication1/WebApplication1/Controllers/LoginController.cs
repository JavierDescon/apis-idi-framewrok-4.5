﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [AllowAnonymous]
    public class LoginController : ApiController
    {
        List<Login> listaUsuarios;
        public LoginController()
        {
            listaUsuarios = new List<Login>();
            //Usuarios con acceso, para generacion de token
            listaUsuarios.Add(new Login("Javier", "jav3097."));
            listaUsuarios.Add(new Login("Claudia", "Admin123"));
        }

        // GET: api/Login/5
        public string Get(int id)
        {
            return "value";
        }


        [HttpPost]
        public Resultado Authenticate([FromBody] Login usuario)
        {
            Resultado resultado = new Resultado();
            try
            {
                var use = listaUsuarios.SingleOrDefault(x => x.Username == usuario.Username && x.Password == usuario.Password);

                if (use != null)
                {
                    string token = createToken(usuario.Username);
                    //retorna el token generado
                    resultado.error = false;
                    resultado.message = token;
                }
                else
                {
                    resultado.error = true;
                    resultado.message="Usuario y/o contraseña incorrecta";
                }
            }
            catch (Exception ex)
            {
                resultado.error = true;
                resultado.message = ex.Message;
            }

            return resultado;
           
        }


        private string createToken(string username)
        {
            DateTime issuedAt = DateTime.UtcNow;
            DateTime expires = DateTime.UtcNow.AddDays(7);
            var tokenHandler = new JwtSecurityTokenHandler();

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, username)
            });

            //Se definen los paramtros de seguridad del token
            const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
            var now = DateTime.UtcNow;
            var securityKey = new SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);


            //Crea el Token
            var token = tokenHandler.CreateJwtSecurityToken(issuer: "issuer", audience: "issuer",
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }

    }
}
